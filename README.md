# interview-task-02

Решение по заданию развернуть и настроить nginx сервер с помощью ansible:
- на linux машине
- в kubernetes кластере (*). Ingress контроллер отсутствует в данном решении, для теста используется "kubectl port-forward service/nginx-service 20118:7778 --address='0.0.0.0'".


Solved task of deploying and configuring of nginx server using ansible:
- deploying on linux vm
- deploying on kubernetes. Ingress controller is not present at this solution, needs to use "kubectl port-forward service/nginx-service 20118:7778 --address='0.0.0.0'".
